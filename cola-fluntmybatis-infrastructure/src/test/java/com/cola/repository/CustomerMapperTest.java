package com.cola.repository;

import com.cola.config.DataSourceConfig;
import com.cola.entity.CargoEntity;
import com.cola.mapper.CargoMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataSourceConfig.class)
public class CustomerMapperTest {

    @Autowired
    CargoMapper mapper;

    @Test
    public void testFindByID() {
        CargoEntity cargo = new CargoEntity().setBookingId("1");
        int insert = mapper.insert(cargo);
        CargoEntity one = mapper.findOne(mapper.query().where().bookingId().eq("1").end());
        System.out.println(one);
        System.out.println("Write your test here");
    }
}
