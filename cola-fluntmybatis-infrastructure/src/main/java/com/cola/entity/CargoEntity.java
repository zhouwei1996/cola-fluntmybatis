package com.cola.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * CargoEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "cargo",
    schema = "ddd"
)
public class CargoEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /**
   * 主键自增
   */
  @TableId("ID")
  private Integer id;

  /**
   * 预订数量
   */
  @TableField("BOOKING_AMOUNT")
  private Integer bookingAmount;

  /**
   * 预订ID
   */
  @TableField("BOOKING_ID")
  private String bookingId;

  /**
   * 当前航次编号
   */
  @TableField("current_voyage_number")
  private String currentVoyageNumber;

  /**
   * 处理事件id
   */
  @TableField("handling_event_id")
  private Integer handlingEventId;

  /**
   * 最后处理事件id
   */
  @TableField("last_handling_event_id")
  private Integer lastHandlingEventId;

  /**
   * 最后处理事件位置
   */
  @TableField("last_handling_event_location")
  private String lastHandlingEventLocation;

  /**
   * 最后处理事件类型
   */
  @TableField("last_handling_event_type")
  private String lastHandlingEventType;

  /**
   * 最后处理事件航次
   */
  @TableField("last_handling_event_voyage")
  private String lastHandlingEventVoyage;

  /**
   * 最后位置id
   */
  @TableField("last_known_location_id")
  private String lastKnownLocationId;

  /**
   * 下一个处理事件类型
   */
  @TableField("next_expected_handling_event_type")
  private String nextExpectedHandlingEventType;

  /**
   * 下一个期望位置id
   */
  @TableField("next_expected_location_id")
  private String nextExpectedLocationId;

  /**
   * 下一个航次id
   */
  @TableField("next_expected_voyage_id")
  private String nextExpectedVoyageId;

  /**
   * 起源id
   */
  @TableField("origin_id")
  private String originId;

  /**
   * 路由状态
   */
  @TableField("ROUTING_STATUS")
  private String routingStatus;

  /**
   * 规范到达最后期限
   */
  @TableField("SPEC_ARRIVAL_DEADLINE")
  private Date specArrivalDeadline;

  /**
   * 规范目的地id
   */
  @TableField("spec_destination_id")
  private String specDestinationId;

  /**
   * 规范起源id
   */
  @TableField("spec_origin_id")
  private String specOriginId;

  /**
   * 运输状态
   */
  @TableField("TRANSPORT_STATUS")
  private String transportStatus;

  @Override
  public final Class entityClass() {
    return CargoEntity.class;
  }
}
