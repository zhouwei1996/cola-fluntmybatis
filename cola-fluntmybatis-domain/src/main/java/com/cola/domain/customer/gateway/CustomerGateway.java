package com.cola.domain.customer.gateway;

import com.cola.domain.customer.Customer;

public interface CustomerGateway {
    public Customer getByById(String customerId);
}
