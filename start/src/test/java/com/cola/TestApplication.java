package com.cola;

import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import com.cola.config.DataSourceConfig;
import com.cola.entity.CargoEntity;
import com.cola.mapper.CargoMapper;
import org.apache.ibatis.annotations.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataSourceConfig.class)
public class TestApplication {

    public static void main(String[] args) {
        //这里填的是TestApplication
        ApplicationContext context = SpringApplication.run(Application.class, args);
        //IMapper mapper = (IMapper)context.getBean("fmCargoMapper");
        //CargoEntity cargo = new CargoEntity().setBookingId("1");
        //int insert = mapper.insert(cargo);
        //CargoEntity one = mapper.findOne(mapper.query().where().bookingId().eq("1").end());
        //System.out.println(one);
    }

    @Autowired
    CargoMapper mapper;

    @Test
    public void testFindByID() {
        CargoEntity cargo = new CargoEntity().setBookingId("1");
        int insert = mapper.insert(cargo);
        CargoEntity one = mapper.findOne(mapper.query().where().bookingId().eq("1").end());
        System.out.println(one);
        System.out.println("Write your test here");
    }


}
