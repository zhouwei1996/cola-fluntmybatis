package com.cola.test;

import com.alibaba.cola.dto.Response;
import com.cola.api.CustomerServiceI;
import com.cola.dto.CustomerAddCmd;
import com.cola.dto.data.CustomerDTO;
import com.cola.dto.data.ErrorCode;
import com.cola.Application;
import com.cola.entity.CargoEntity;
import com.cola.mapper.CargoMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * This is for integration test.
 *
 * Created by fulan.zjf on 2017/11/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CustomerServiceTest {

    @Autowired
    private CustomerServiceI customerService;


    @Before
    public void setUp() {

    }
    @Autowired
    CargoMapper mapper;

    @Test
    public void testFindByID() {
        CargoEntity cargo = new CargoEntity().setBookingId("1");
        int insert = mapper.insert(cargo);
        CargoEntity one = mapper.findOne(mapper.query().where().bookingId().eq("1").end());
        System.out.println(one);
        System.out.println("Write your test here");
    }

    @Test
    public void testCustomerAddSuccess(){
        //1.prepare
        CustomerAddCmd customerAddCmd = new CustomerAddCmd();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCompanyName("NormalName");
        customerAddCmd.setCustomerDTO(customerDTO);

        //2.execute
        Response response = customerService.addCustomer(customerAddCmd);

        //3.assert
        Assert.assertTrue(response.isSuccess());
    }

    @Test
    public void testCustomerAddCompanyNameConflict(){
        //1.prepare
        CustomerAddCmd customerAddCmd = new CustomerAddCmd();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCompanyName("ConflictCompanyName");
        customerAddCmd.setCustomerDTO(customerDTO);

        //2.execute
        Response response = customerService.addCustomer(customerAddCmd);

        //3.Exception
        Assert.assertEquals(ErrorCode.B_CUSTOMER_companyNameConflict, response.getErrCode());

    }
}
